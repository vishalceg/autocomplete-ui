import React from 'react';

// Import the Autocomplete Component
import Autocomplete from 'react-autocomplete';

export default class App extends React.Component {

    constructor(props, context) {
        super(props, context);

        // Set initial State
        this.state = {
            // Current value of the select field
            value: "",
            autocompleteData: [] // sugges

        };

        this.onChange = this.onChange.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.getItemValue = this.getItemValue.bind(this);
        this.renderItem = this.renderItem.bind(this);
    }

    onChange(e) {
        this.setState({
            value: e.target.value
        });

        if (e.target.value === "") {
            this.setState({
                autocompleteData: []
            });
        } else {
            this.userList(e.target.value);
        }

        console.log("The Input Text has changed to ", e.target.value);
    }

    onSelect(val) {
        this.setState({
            value: val
        });

        console.log("Option from 'database' selected : ", val);
    }

    renderItem(item, isHighlighted) {
        return ( <
            div style = {
                {
                    background: isHighlighted ? 'lightgray' : 'white'
                }
            } > {
                item
            } <
            /div>   
        );
    }

    getItemValue(item) {
        return '${item}';
    }

    userList(value) {
        console.log('inside ');
        fetch("http://localhost:6060/api/v1/search" + "?data=" + value)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({
                    autocompleteData: data
                });
            });
    }



    render() {
        console.log("Tests ", this.state.value);
        return ( <
            div >
            <
            Autocomplete getItemValue = {
                this.getItemValue
            }
            items = {
                this.state.autocompleteData
            }
            renderItem = {
                this.renderItem
            }
            value = {
                this.state.value
            }
            onChange = {
                this.onChange
            }
            onSelect = {
                this.onSelect
            }
            /> <
            /div>
        );
    }
}